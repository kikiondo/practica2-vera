//..................................................................
//.VVVVVV.........VVVVVV............................................
//.VVVVVV........VVVVVV.............................................
//.VVVVVV........VVVVVV.............................................
//.VVVVVVV.......VVVVVV.............................................
//..VVVVVV......VVVVVV..............................................
//..VVVVVV......VVVVVV..............................................
//..VVVVVVV....VVVVVV....eeeeeeeee.....errrrrrrrrrr..aaaaaaaaaa.....
//...VVVVVV....VVVVVV...eeeeeeeeeeee...errrrrrrrrrr.aaaaaaaaaaaa....
//...VVVVVV....VVVVVV..Veeeeeeeeeeee...errrrrrrrrr.raaaaaaaaaaaaa...
//...VVVVVVV..VVVVVV..VVeeeeeeeeeeeee..errrrrrr....raaaaaaaaaaaaa...
//....VVVVVV..VVVVVV..VVeeee...eeeeee..errrrrr.....raaaa...aaaaaa...
//....VVVVVV..VVVVVV..VVeee.....eeeeee.errrrr...............aaaaa...
//....VVVVVVVVVVVVV...VVeeeeeeeeeeeeee.errrrr...........aaaaaaaaa...
//.....VVVVVVVVVVVV...VVeeeeeeeeeeeeee.errrrr.......aaaaaaaaaaaaa...
//.....VVVVVVVVVVV....VVeeeeeeeeeeeeee.errrrr......raaaaaaaaaaaaa...
//......VVVVVVVVVV....VVeee............errrrr.....rraaaaaaaaaaaaa...
//......VVVVVVVVVV....VVeeee...........errrrr.....rraaaa...aaaaaa...
//......VVVVVVVVV.....VVeeee...........errrrr.....rraaaa...aaaaaa...
//.......VVVVVVVV.....VVeeeeeeeeeeeee..errrrr.....rraaaaaaaaaaaaa...
//.......VVVVVVVV......Veeeeeeeeeeeee..errrrr.....rraaaaaaaaaaaaa...
//.......VVVVVVV........eeeeeeeeeeeee..errrrr......raaaaaaaaaaaaaa..
//........VVVVVV.........eeeeeeeeee....errrrr.......aaaaaaaaaaaaaa..
//..................................................................

Welcome to your Node.js project on Cloud9 IDE!

This chat example showcases how to use `socket.io` with a static `express` server.

## Running the server

1) Open `server.js` and start the app by clicking on the "Run" button in the top menu.

2) Alternatively you can launch the app from the Terminal:

    $ node server.js

Once the server is running, open the project in the shape of 'https://projectname-username.c9users.io/'. As you enter your name, watch the Users list (on the left) update. Once you press Enter or Send, the message is shared with all connected clients.
